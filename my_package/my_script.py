import os

def my_function():
    print("started my function")

    print("GITLAB_USER_NAME:",end="")
    print(os.environ.get("GITLAB_USER_NAME"))

    print("my_gitlab_cicd_variable_key:",end="")
    print(os.environ.get("my_gitlab_cicd_variable_key"))

    print("my_gitlab_ci_yml_variable_key:",end="")
    print(os.environ.get("my_gitlab_ci_yml_variable_key"))

    print("my_image_env_var_key:",end="")
    print(os.environ.get("my_image_env_var_key"))

    print("my_container_env_var_key:",end="")
    print(os.environ.get("my_container_env_var_key"))

    print("finished my function")
