FROM python:3.6-stretch
ENV my_image_env_var_key=my_image_env_var_value
RUN mkdir mycontainerdir
COPY ./my_package/my_script.py mycontainerdir
CMD ["python", "mycontainerdir/my_script.py"]