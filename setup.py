from setuptools import find_packages, setup
setup(
    name="learnfromscratch",
    packages=find_packages(),
    entry_points={
        "console_scripts": [
            "learnfromscratch=my_package.my_script:my_function"
        ]
    }
)